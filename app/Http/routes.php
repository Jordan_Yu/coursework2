<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'QuestionnaireController');

Route::resource('/users', 'userController');

Route::resource('/list', 'ListController');

Route::resource('/{id}', 'QuestionnaireController@show');

Route::group(['middleware' => ['web']], function () {
    Route::resource('tests', 'QuestionnaireController');
});