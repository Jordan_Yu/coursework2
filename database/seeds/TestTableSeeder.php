<?php

use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests')->insert([
            ['id' => 1, 'name' => 'Initial seeder test', 'description' => 'This is a test for the seeder', 'result' => 'This seeder should work as intended'],
            ['id' => 2, 'name' => 'Second seeder test', 'description' => 'This test is a repeat of the first one to populate the database', 'result' => 'It should work as it did the first time'],
            ['id' => 3, 'name' => 'Third test', 'description' => 'This test is to add more data to the database ', 'result' => 'It worked previously so it should work again'],
        ]);
    }
}
