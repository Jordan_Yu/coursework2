<!doctype html>
<html lang="en">
<head>
    <title>Users</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>List of users</h1>
<p>This is a list of all registered users</p>
<section>
    @if (isset ($users))
        <ul>
            @foreach ($users as $user)
                <li>{{ $user->name }}</li>
            @endforeach
        </ul>
        @else
            <p>There are no users</p>
        @endif

    <h1>Description test</h1>
        @if (isset ($users))
            <ul>
                @foreach ($users as $user)
                    <li>{{ $user->description }}</li>
                @endforeach
            </ul>
        @else
            <p>There are no users</p>
        @endif
</section>
</body>
</html>