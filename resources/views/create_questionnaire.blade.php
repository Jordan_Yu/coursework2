<!doctype html>
<html lang="en">
<head>
    <title>Create Questionnaire</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
@include ('includes/header')
<article class="row">
    <article stlye ='margin-top: 50px;'class="container">
    <h1>Create a new questionnaire</h1>
    @if ($errors->any())
        <div>
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => '/']) !!}

    <div class="form-group">
        {!! Form::label('name', 'Title:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Topic:') !!}
        {!! Form::select('description', array('School' => 'School','Entertainment' => 'Entertainment', 'Lifestyle' => 'Lifestyle'), null,['placeholder' => 'Select...']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('result', 'Description:') !!}
        {!! Form::textarea('result', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
    </div>


    {!! Form::close() !!}
   </article>
</article>
</body>
</html>