<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of Questionnaires</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
@include('includes/header')
<section>
        <div id='container'>
            <h1>List of Questionnaires</h1>

            @if (isset($questions))
                @foreach($questions as $question)
                    <ul>
                        <h2>{{ $question->name }}</h2>
                        <li>{{ $question->description }}</li>
                        <li>{{ $question->result }}</li>
                        <li>Date created:{{ $question->created_at }}</li>
                    </ul>
                @endforeach
            @else
                <p>This feature does not work as intended</p>
            @endif
        </div>
</section>
</body>
</html>