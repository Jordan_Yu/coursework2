<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Homepage</title>
    <link rel="stylesheet" href="/css/app.css" />

    <style>
        body{
            background-color:grey;
        }

        ul{
            list-style-type: none;
        }

        li{
            margin-top: 10px;
            display: inline-block;
            padding-right: 10px;
        }

        h2{
            margin: 0;
            text-decoration:none;
        }


    </style>
</head>
<body>
   @include ('includes/header')
   <h1>Questionnaires</h1>
   <section>
       @if (isset ($users))
           <ul>
               @foreach ($users as $user)
                   <li>{{ $user->name }}</li>
               @endforeach
           </ul>
       @else
           <p>There are no users</p>
       @endif

       <h1>Description test</h1>
       @if (isset ($users))
           <ul>
               @foreach ($users as $user)
                   <li>{{ $user->description }}</li>
               @endforeach
           </ul>
       @else
           <p>There are no users</p>
       @endif
           <div id='container'>
               <h1>List of Questionnaires</h1>

        @if (isset($users))
           @foreach($users as $user)
               <ul>
                   <h2><a href="/{{ $user->id }}" name="{{  $user->name}}"> {{ $user->name }} </a></h2>
                   <li>{{ $user->description }}</li>
                   <li>{{ $user->result }}</li>
                   <li>Date created:{{ $user->created_at }}</li>
                   <li>{!! Form::open(['method' => 'DELETE', 'route' => ['tests.destroy', $user->id]]) !!}
                   {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                   {!! Form::close() !!}</li>
               </ul>
            @endforeach
                   @else
               <p>This feature does not work as intended</p>
                   @endif
           </div>
   </section>
</body>
</html>